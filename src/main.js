"use strict";
require('./polyfills');
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var main_module_1 = require('./modules/main.module');
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(main_module_1.MyModule)
    .then(function (success) { return console.log("Bootstrap success"); })
    .catch(function (error) { return console.error(error); });
//# sourceMappingURL=main.js.map