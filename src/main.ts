import './polyfills';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MyModule } from './modules/main.module';

platformBrowserDynamic().bootstrapModule(MyModule)
	.then(success => console.log(`Bootstrap success`))
	.catch(error => console.error(error));
